# NestJS

<ul>
    <li><b>Module</b> - изолированная сущность NestJS(пока модуль нигде не зарагистрирован он "висит в воздухе")</li>
    <li><b>Controller</b> - описывает роуты NestJS(пока контроллер нигде не зарагистрирован он "висит в воздухе"),
        контроллер мы не можем переиспользовать</li>
    <li>
        <b>Service</b> - описывает экшены(методы работы с бд и не только) NestJS(пока сервис нигде не зарагистрирован он "висит в воздухе")
        сервисы могут переиспользоваться в разных контроллерах
    </li>
</ul>

# Problems

<ul>
    <li>WebStorm для исправления eslint - "eslint": "8.22.0"</li>
</ul>

# Base info

<ul>
    <li><b>DTO</b> - это классы которые описываю боди передаваемое в методы</li>
</ul>

# Base common info

<ul>
    <li><a href="https://developer.mozilla.org/ru/docs/Web/HTTP/Status">Справочник по статус кодам HTTP</a></li>
</ul>

# Linters

<ul>
    <li>
        <b>.eslintrc.js</b>
        <pre>
            module.exports = {
              parser: '@typescript-eslint/parser',
              parserOptions: {
                project: 'tsconfig.json',
                tsconfigRootDir : __dirname,
                sourceType: 'module',
              },
              plugins: ['@typescript-eslint/eslint-plugin'],
              extends: [
                'plugin:@typescript-eslint/recommended',
                'plugin:prettier/recommended',
              ],
              root: true,
              env: {
                node: true,
                jest: true,
              },
              ignorePatterns: ['.eslintrc.js'],
              rules: {
                '@typescript-eslint/interface-name-prefix': 'off',
                '@typescript-eslint/explicit-function-return-type': 'off',
                '@typescript-eslint/explicit-module-boundary-types': 'off',
                '@typescript-eslint/no-explicit-any': 'off',
                '@typescript-eslint/no-empty-interface': 'off'
              },
            };
        </pre>
    </li>
    <li>
        <b>tsconfig.json</b>
        <pre>
            {
              "compilerOptions": {
                "module": "commonjs",
                "declaration": true,
                "removeComments": true,
                "emitDecoratorMetadata": true,
                "experimentalDecorators": true,
                "allowSyntheticDefaultImports": true,
                "target": "es2017",
                "sourceMap": true,
                "outDir": "./dist",
                "baseUrl": "./",
                "incremental": true,
                "skipLibCheck": true,
                "strictNullChecks": true,
                "noImplicitAny": true,
                "strictPropertyInitialization": false,
                "strictBindCallApply": false,
                "forceConsistentCasingInFileNames": false,
                "noFallthroughCasesInSwitch": false,
              }
            }
        </pre>
    </li>
    <li>
        <b>.prettierrc</b>
        <pre>
            {
              "singleQuote": true,
              "trailingComma": "all",
              "endOfLine":"auto"
            }
        </pre>
    </li>
</ul>

# Docker commands

<ul>
    <li><b>docker-compose up --build</b> - собрать и запустить</li>
    <li>
      <b>docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build</b> - данный способ:
      <p>берет все поля из docker-compose.yml и переписывает их полями из docker-compose.dev.yml</p>
    </li>
</ul>